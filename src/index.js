var cron = require('node-cron');
class CronJob {
    cron = false;
    crons = []
    options = false
    jobs = []
    constructor(){
        this.cron = cron
    }

    add({in_seconds,uniq,exec,sendData,max,execSync}){
       if(in_seconds && !this.jobs[in_seconds]){
            in_seconds  = this.in_seconds(in_seconds)
           
            this.jobs[in_seconds] = {
                in_seconds,
                uniq,
                running : false,
                exec,
                execSync,
                sendData,
                max
            }
            
       }

    }
    stop(value){
        this.crons[value].destroy();
    }

    schedule(value){
       this.crons[value] =  this.cron.schedule(value, async() => {
            var check = this.jobs[value]
           
            if(!check)return
          
            if(check.exec || check.execSync){
                if(check.uniq && check.running){
                   
                    return;
                }
                check.running = true
                if(check.exec)
                    check.exec(check.sendData)
                if(check.execSync)
                    await check.execSync(check.sendData)
                check.running = false
                check.count = (check.count?check.count:0) + 1
               
                if(check.max <= check.count){
                    this.stop(value)
                }
            }
           
        });

        return value
    }
    all_minutes(value='*'){
        
        return this.schedule(`${value} * * * *`)

    }
    in_minutes(value){
        return this.all_minutes(`*/${value}`)
    }
    in_seconds(value){

        return this.all_minutes(`*/${value} *`)
    }
    
}
module.exports = new CronJob()