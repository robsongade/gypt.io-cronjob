const CronJob = require('./../src/index.js')

CronJob.add({
    in_seconds : 5,
    uniq:true,
    sendData : {
        name : "John"
    },
    max : 2,
    execSync :  async(data) => {
        console.log('1 sec name',data,new Date())
        await new Promise(resolve => setTimeout(resolve, 3000));

        console.log('2 sec name',data,new Date())
    }
})